package com.getbeautify.beautify;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public abstract class MenuUtils {

	/* public methods */

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static void SetupActionBar(Activity activity) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			activity.getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	public static boolean OptionsItemSelected(MenuItem item, Activity activity) {
	    // Handle presses on the action bar items
		boolean processed = false;
	    switch (item.getItemId()) {
     		case android.R.id.home:
     			NavUtils.navigateUpFromSameTask(activity);
     			processed = true;
	        case R.id.action_about:
	            openAbout();
	            processed = true;
	            break;
	        case R.id.action_settings:
	            openSettings();
	            processed = true;
	            break;
	        case R.id.action_signout:
	            processed = true;
	        	signOut(activity);
	    }
	    return processed;
	}

	
	/* private methods */

	private static void openAbout() {
		
	}
	
	private static void openSettings() {
		
	}
	
	private static void signOut(Context context) {
		HttpUtils.SignOut(context);
		
		Intent intent = new Intent(context, MainActivity.class);
		context.startActivity(intent);
	}
}
