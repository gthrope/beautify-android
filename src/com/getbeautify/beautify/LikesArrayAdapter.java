package com.getbeautify.beautify;

import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class LikesArrayAdapter extends ArrayAdapter<JSONObject> implements OnItemClickListener {

    private MyLikesActivity myLikesActivity;
    private int resource;
    
    public LikesArrayAdapter(MyLikesActivity myLikesActivity, int resource, JSONObject[] likes) {
        super(myLikesActivity, resource, likes);
        this.myLikesActivity = myLikesActivity;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        // get inflater
        LayoutInflater inflater = (LayoutInflater) myLikesActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate view for single like
        View likeItem = inflater.inflate(resource, parent, false);

        // get individual fields from the like view
        TextView titleTextView = (TextView) likeItem.findViewById(R.id.likeTitle);
        TextView artistTextView = (TextView) likeItem.findViewById(R.id.likeArtist);
        
        JSONObject like = getItem(position);
        try {
            // parse json
            String title = (String)like.getString("name");
            String artist = (String)like.getString("artist");

            // set fields in the like view
            titleTextView.setText(title);
            artistTextView.setText(artist);
        }
        catch (Exception e) {
            Log.d("Error", e.toString());
        }

        return likeItem;
    }

    // not sure if this belongs here
    // clean up arguments. need to cast arg0?
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        Intent intent = new Intent(myLikesActivity, SingleLikeActivity.class);
        JSONObject like = (JSONObject)arg0.getItemAtPosition(arg2);
        try {
            // parse json
            String title = (String)like.getString("name");
            String artist = (String)like.getString("artist");
            String imageUrl = (String)like.getString("image_url");
            String buyUrl = (String)like.getString("buy_url");

            // set fields in the intent
            intent.putExtra(MyLikesActivity.ARTIST, artist);
            intent.putExtra(MyLikesActivity.TITLE, title);
            intent.putExtra(MyLikesActivity.IMAGE_URL, imageUrl);
            intent.putExtra(MyLikesActivity.BUY_URL, buyUrl);
        }
        catch (Exception e) {
            Log.d("Error", e.toString());
        }
        
        myLikesActivity.startActivity(intent);
    }
    
}
