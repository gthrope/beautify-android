package com.getbeautify.beautify;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(HttpUtils.IsSignedIn(this)) {
        	Intent intent = new Intent(this, MyLikesActivity.class);
        	startActivity(intent);
        }
        else {
            setContentView(R.layout.activity_main);
        }
    }    
    
    public void navSignIn(View view) {
    	Intent intent = new Intent(this, SignInActivity.class);
    	startActivity(intent);
    }
    
    public void navSignUp(View view) {
    }
    
    /* menu stuff */
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.not_signed_in, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	boolean processed = MenuUtils.OptionsItemSelected(item, this);
    	if(!processed) {
    		processed = super.onOptionsItemSelected(item);
    	}
    	return processed;
    }
    
}
