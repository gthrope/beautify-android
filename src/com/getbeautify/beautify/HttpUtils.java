package com.getbeautify.beautify;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;


public abstract class HttpUtils {

	private static final boolean dev = false;	

	/* public methods */
	
	public static boolean PerformUserAuthentication(String email, String password, Context context) {
		
		boolean userAuthenticated = false;
		HttpURLConnection urlConnection = null;
		
		try {
			JSONObject container = new JSONObject();
			JSONObject session = new JSONObject();
			
			session.put("email", email);
			session.put("password", password);
			container.put("session", session);
			
			URL url = buildUrl("/sessions.json");
			urlConnection = (HttpURLConnection) url.openConnection();
			addJsonToBody(urlConnection, container);
			
			int responseCode = urlConnection.getResponseCode();

			if(responseCode == 200) {
				userAuthenticated = true;
				String rememberToken = readResponseBody(urlConnection);
				storeRememberToken(rememberToken, context);
			}
		}
		catch (Exception e) {
			Log.d("glenn", e.toString());
			// TODO: something
		}
		finally {
			if(urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		
		return userAuthenticated;		
	}
	
	public static String GetLikes(Context context) {
		return GetJson(context, "/mylikes.json");
	}
	
	public static String GetChromeBrowse(Context context) {
		return GetJson(context, "/chromebrowse.json");
	}
	
	public static boolean IsSignedIn(Context context) {
		SharedPreferences prefs = context.getSharedPreferences("MyPrefs", 0);
		String rememberToken = prefs.getString("rememberToken", null);
		return rememberToken != null;
	}
	
	public static void SignOut(Context context) {
		SharedPreferences prefs = context.getSharedPreferences("MyPrefs", 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("rememberToken", null);
		editor.commit();
	}
	
	/* private methods */
	
	private static String GetJson(Context context, String path) {
		String response = null;
		HttpURLConnection urlConnection = null;
		
		try {
			URL url = buildUrl(path);
			urlConnection = (HttpURLConnection) url.openConnection();
			addRememberTokenToRequest(urlConnection, context);
			response = readResponseBody(urlConnection);
		}
		catch (Exception e) {
            Log.e("Error", e.getLocalizedMessage());
		}
		finally {
			if(urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		
		return response;
	}
		
	private static URL buildUrl(String path) {
		// build URL
		URL url = null;
		try {
			if(dev) {
				url = new URL("http", "10.0.2.2", 3000, path);
			}
			else {
				url = new URL("https", "www.getbeautify.com", path);
			}
		}
		catch (Exception e) {
			// TODO: something!
		}
		return url;
	}
	
	private static void addJsonToBody(HttpURLConnection urlConnection, JSONObject json) {
		// TODO: not sure this belongs here... exceptions...
		try {
			byte[] jsonBytes = json.toString().getBytes(HTTP.UTF_8);

			// set content type to json; can skip "Accept" because path should end with .json
			urlConnection.setRequestProperty("Content-type", "application/json");
	
			// set request to POST and let us add a body
			urlConnection.setDoOutput(true);
	
			// add body
			urlConnection.setFixedLengthStreamingMode(jsonBytes.length);
			OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
			out.write(jsonBytes);
			out.flush();
		}
		catch (Exception e) {
			// TODO: something!
		}
	}
	
	private static String readResponseBody(HttpURLConnection urlConnection) {
		String responseBody = null;
		
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), HTTP.UTF_8));
			String line = null;
			StringBuilder sb = new StringBuilder();
			while((line = in.readLine()) != null) {
				sb.append(line);
			}
			responseBody = sb.toString();
		}
		catch (Exception e) {
            Log.e("Error", e.getLocalizedMessage());
		}
		
		return responseBody;
	}
	
	private static void storeRememberToken(String rememberToken, Context context) {
		SharedPreferences prefs = context.getSharedPreferences("MyPrefs", 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("rememberToken", rememberToken);
		editor.commit();
	}
	
	private static void addRememberTokenToRequest(HttpURLConnection urlConnection, Context context) {
		SharedPreferences prefs = context.getSharedPreferences("MyPrefs", 0);
		String rememberToken = prefs.getString("rememberToken", null);
		urlConnection.setRequestProperty("Cookie", "remember_token=" + rememberToken);
	}
	
}
