package com.getbeautify.beautify.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import com.getbeautify.beautify.R;
import com.getbeautify.beautify.backgroundchanger.LockScreenIntentService;

public class BeautifyWidgetProvider extends AppWidgetProvider {

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		final int numWidgets = appWidgetIds.length;	// necessary?
		for(int i=0; i<numWidgets; i++) {
			int appWidgetId = appWidgetIds[i];

			PendingIntent likePendingIntent = getPendingIntent(context, LockScreenIntentService.LIKE_INTENT);
			PendingIntent nextPendingIntent = getPendingIntent(context, LockScreenIntentService.NEXT_INTENT);

			RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.beautify_widget);
			views.setOnClickPendingIntent(R.id.widget_button_like, likePendingIntent);
			views.setOnClickPendingIntent(R.id.widget_button_next, nextPendingIntent);

			appWidgetManager.updateAppWidget(appWidgetId, views);
		}
	}
	
	private PendingIntent getPendingIntent(Context context, String action) {
		PendingIntent pendingIntent = null;
		try {
			Intent intent = new Intent(context, LockScreenIntentService.class);
			intent.setAction(action);
			pendingIntent = PendingIntent.getService(context, 0, intent, 0);
		}
		catch(Exception e) {
			Log.d("Error", e.toString());
		}
		return pendingIntent;
	}
	
}
