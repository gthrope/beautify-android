package com.getbeautify.beautify;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class BuyActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy);
        // Show the Up button in the action bar.
        MenuUtils.SetupActionBar(this);
        
        Intent intent = getIntent();
        String artist = intent.getStringExtra(MyLikesActivity.ARTIST);
        String buyUrl = intent.getStringExtra(MyLikesActivity.BUY_URL);

        setTitle(artist);
        WebView buyWebView = (WebView) findViewById(R.id.buyWebView);
        buyWebView.loadUrl(buyUrl);
        
        // is this stuff right/necessary?
        // do this before loadUrl?
        
        // enable javascript
        WebSettings webSettings = buyWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        
        // open links inside app
        buyWebView.setWebViewClient(new WebViewClient());
    }

    /* menu stuff */
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.signed_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean processed = MenuUtils.OptionsItemSelected(item, this);
        if(!processed) {
            processed = super.onOptionsItemSelected(item);
        }
        return processed;
    }
}
