package com.getbeautify.beautify.backgroundchanger;

import java.io.InputStream;
import java.net.URL;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.getbeautify.beautify.HttpUtils;

public class LockScreenIntentService extends IntentService {
	
	public static final String LIKE_INTENT = "LikeIntent";
	public static final String NEXT_INTENT = "NextIntent";
	public static final String LOCK_INTENT = "LockIntent";
	
    public LockScreenIntentService() {
        super("LockScreenIntentService");
    }
    
    
    @SuppressLint("NewApi") @Override
    protected void onHandleIntent(Intent intent) {
    	String action = intent.getAction();
    	
    	if(action == NEXT_INTENT || action == LOCK_INTENT) {
	    	try
	        {
	            // not sure about this...
	            // check for intenet connection?
	            // do I need to create bitmap, or can I call setStream?
	            // do I need to close stuff better? finally?
	            
	            // get image info
	            String response = HttpUtils.GetChromeBrowse(this);
	
	            // parse json to get image url
	            JSONObject json = new JSONObject(response);
	            String imagePath = json.getString("image_url");
	            
	            // get image
	            URL imageUrl = new URL(imagePath);
	            InputStream imageInputStream = imageUrl.openStream();
	            Bitmap content = BitmapFactory.decodeStream(imageInputStream);
	            imageInputStream.close();
	            
	            // resize image and set as background
	            WallpaperManager wm = WallpaperManager.getInstance(this);
	            int width = wm.getDesiredMinimumWidth();
	            int height = wm.getDesiredMinimumHeight();
	            Bitmap resizedContent = Bitmap.createScaledBitmap(content, width, height, true);
	            wm.setBitmap(resizedContent);
	        }
	        catch(Exception e)
	        {
	            Log.e("Error", e.getLocalizedMessage());
	        }
            finally
            {

            }
    	}
    	else if(action == LIKE_INTENT) {
    		
    	}
    }

}
