package com.getbeautify.beautify.backgroundchanger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class LockScreenBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent lockScreenIntent = new Intent(context, LockScreenIntentService.class);
        lockScreenIntent.setAction(LockScreenIntentService.LOCK_INTENT);
        context.startService(lockScreenIntent);
    }
}
