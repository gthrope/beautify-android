package com.getbeautify.beautify;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SignInActivity extends Activity {

	//  Keep track of the login task to ensure we can cancel it if requested
	private UserLoginTask mAuthTask = null;
	
	// Values for email and password at time of login attempt
	private String mEmail;
	private String mPassword;

	// UI references
	private View mLoginFormView;
	private View mLoginStatusView;
	private EditText mEmailView;
	private EditText mPasswordView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_in);
		// Show the Up button in the action bar.
		MenuUtils.SetupActionBar(this);

		mLoginFormView = (View) findViewById(R.id.login_form);
		mLoginStatusView = (View) findViewById(R.id.login_status);
		
		mEmailView = (EditText) findViewById(R.id.email);
		mPasswordView = (EditText) findViewById(R.id.password);
		Button loginButton = (Button) findViewById(R.id.sign_in_button);
				
		mPasswordView
			.setOnEditorActionListener(new TextView.OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
					if(id == R.id.login || id == EditorInfo.IME_NULL) {
						attemptLogin();
						return true;
					}
					return false;
				}
			});
		
		loginButton.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						attemptLogin();
					}
				});
	}
	
	public void attemptLogin() {
		if(mAuthTask != null) {
			return;
		}
		
		// reset errors
		mEmailView.setError(null);
		mPasswordView.setError(null);
		
		// store values at the time of the login attempt
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();
		
		boolean cancel = false;
		View focusView = null;
		
		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 6) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}
		
		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} else if (!mEmail.contains("@")) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}
		
		if(cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		}
		else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			showProgress(true);
			mAuthTask = new UserLoginTask(this);
			mAuthTask.execute((Void) null);
		}				
	}
	
	private void showProgress(final boolean show) {
		// TODO - look at animations here
		mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		mLoginStatusView.setVisibility(show? View.VISIBLE : View.GONE);
	}
	
	private class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

		private SignInActivity mSignInActivity;
		
		public UserLoginTask(SignInActivity signInActivity) {
			super();
			mSignInActivity = signInActivity;
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			return HttpUtils.PerformUserAuthentication(mEmail, mPassword, mSignInActivity);
		}
		
		@Override
		protected void onPostExecute(final Boolean userAuthenticated) {
			mAuthTask = null;
			showProgress(false);
			
			if(userAuthenticated) {
//				finish();
				Intent intent = new Intent(mSignInActivity, MyLikesActivity.class);
				startActivity(intent);
			}
			else {
				mPasswordView.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
			}
		}
		
		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
	
	
	/* menu stuff */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.not_signed_in, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	boolean processed = MenuUtils.OptionsItemSelected(item, this);
    	if(!processed) {
    		processed = super.onOptionsItemSelected(item);
    	}
    	return processed;
    }
}
