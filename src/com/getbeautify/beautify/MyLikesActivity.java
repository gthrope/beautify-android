package com.getbeautify.beautify;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.getbeautify.beautify.backgroundchanger.LockScreenBroadcastReceiver;

// TODO: should this extend ListActivity? Get stuff for free?
public class MyLikesActivity extends Activity {

    public final static String TITLE = "com.getbeautify.beautify.TITLE";
    public final static String ARTIST = "com.getbeautify.beautify.ARTIST";
    public final static String IMAGE_URL = "com.getbeautify.beautify.IMAGE_URL";
    public final static String BUY_URL = "com.getbeautify.beautify.BUY_URL";
    
    private ListView mListView;
    private GetLikesTask mGetLikesTask;
    private LockScreenBroadcastReceiver mLockScreenBroadcastReceiver;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_likes);
        
        mListView = (ListView) findViewById(R.id.likesList);
        
        mGetLikesTask = new GetLikesTask(this);
        mGetLikesTask.execute((Void) null);

        // set up receiver to listen for lock screen
        mLockScreenBroadcastReceiver = new LockScreenBroadcastReceiver();
        this.registerReceiver(mLockScreenBroadcastReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));
    }
    
    @Override
    protected void onDestroy() {
        this.unregisterReceiver(mLockScreenBroadcastReceiver);
        super.onDestroy();
    }
        
    private class GetLikesTask extends AsyncTask<Void, Void, String> {

        private MyLikesActivity mMyLikesActivity;
        
        public GetLikesTask(MyLikesActivity myLikesActivity) {
            super();
            mMyLikesActivity = myLikesActivity;
        }
        
        @Override
        protected String doInBackground(Void... arg0) {
            return HttpUtils.GetLikes(mMyLikesActivity);
            //return HttpUtils.PerformHttpRequest("mylikes", null, true);
        }
        
        @Override
        protected void onPostExecute(final String mylikes) {
            
            try {
                // create json array
                JSONArray json = new JSONArray(mylikes);

                // convert json array into regular array
                int jsonLength = json.length();
                JSONObject[] jsonArray = new JSONObject[jsonLength];
                for(int i=0; i<jsonLength; i++) {
                    jsonArray[i] = json.getJSONObject(i);
                }
                
                // get list view
                ListView listView = mMyLikesActivity.mListView;
                
                // create list adapter
                // argument 1: context
                // argument 2: resource for individual like
                // argument 3: like data array
                ListAdapter listAdapter = new LikesArrayAdapter(mMyLikesActivity, R.layout.like_item, jsonArray);

                // hook up list view and list adapter
                listView.setAdapter(listAdapter);
                listView.setOnItemClickListener((OnItemClickListener)listAdapter);
            }
            catch(Exception e) {
                Log.d("Error", e.toString());
            }
        }
        
        @Override
        protected void onCancelled() {
            mGetLikesTask = null;
        }
    }
    
    
    /* menu stuff */
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.signed_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean processed = MenuUtils.OptionsItemSelected(item, this);
        if(!processed) {
            processed = super.onOptionsItemSelected(item);
        }
        return processed;
    }

}
