package com.getbeautify.beautify;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

public class SingleLikeActivity extends Activity {

    private String m_artist;
    private String m_title;
    private String m_imageUrl;
    private String m_buyUrl;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_like);
        // Show the Up button in the action bar.
        MenuUtils.SetupActionBar(this);

        Intent intent = getIntent();
        m_artist = intent.getStringExtra(MyLikesActivity.ARTIST);
        m_title = intent.getStringExtra(MyLikesActivity.TITLE);
        m_imageUrl = intent.getStringExtra(MyLikesActivity.IMAGE_URL);
        m_buyUrl = intent.getStringExtra(MyLikesActivity.BUY_URL);
        
        setTitle(m_artist);
        
        // get individual fields from the like view
        TextView titleTextView = (TextView) findViewById(R.id.singleLikeTitle);
        WebView imageWebView = (WebView) findViewById(R.id.singleLikeImage);

        // set fields in the like view
        titleTextView.setText(m_title);
        imageWebView.loadUrl(m_imageUrl);
    }

    public void navBuy(View view) {
        Intent intent = new Intent(this, BuyActivity.class);
        intent.putExtra(MyLikesActivity.ARTIST, m_artist);
        intent.putExtra(MyLikesActivity.BUY_URL, m_buyUrl);
        startActivity(intent);
    }
    
    /* menu stuff */
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.signed_in, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean processed = MenuUtils.OptionsItemSelected(item, this);
        if(!processed) {
            processed = super.onOptionsItemSelected(item);
        }
        return processed;
    }

}
